# notes-to-self
notes on software and system configuration

## ssh hardening

1. if `PermitRootLogin` is set to `yes`, change it to `no` !

2. enable users selectively:

  in [sshd_config](http://www.openbsd.org/cgi-bin/man.cgi?query=sshd_config) set:
  
  `AllowUsers aeuae someuser anotheruser`
  
  or for a system with more than a handful of users:
  
  `AllowGroups ssh-users`
  
3. set allowed *KexAlgorithms*, *Ciphers* and *MACs* in `sshd_config` and `ssh_config`:

  ```
  KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256
  Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
  MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-ripemd160-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,hmac-ripemd160,umac-128@openssh.com
  ```
4. use at least `4096 bit rsa` keys or preferably `ed25519`:

  ```
  ssh-keygen -t ed25519 -o -a 100
  
  ssh-keygen -t rsa -b 4096 -o -a 100
  ```

  and when prompted, provide a passphrase for the key!

5. not for security, but to get less log spam change the default `sshd` port to some other unused port:

  in sshd_config set `Port 35`
  
  or any other port number of your preference and don't forget to open this port in your firewall rules!

more interesting suggestions in this [blog post](https://stribika.github.io/2015/01/04/secure-secure-shell.html)
