

## freebsd: nginx, mariadb and php setup


###### from ports install all nginx, mariadb and php56

``` 
portmaster www/nginx databases/mariadb100-client databases/mariadb100-server lang/php-56 
```

###### update `rc.conf`

mysql_enable="YES"
nginx_enable="YES"
php_fpm_enable="YES"


## configure php

in `/usr/local/etc/php-fpm.conf` change the line

``` listen = 127.0.0.1:9000```
to
``` listen = /var/run/php-fpm.sock```

and uncomment the `listen.owner` section

--work in progress--
